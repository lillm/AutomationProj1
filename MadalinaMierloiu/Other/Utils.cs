﻿using ConsoleApp1.Other.DataMaps;
using MadalinaMierloiu.DataMap;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static MadalinaMierloiu.Other.EnumItems;

namespace MadalinaMierloiu.Other
{
    /// <summary>
    /// configurarea coneziunii de DB
    /// </summary>
    class Utils
    {
        public static string msConString = String.Format("Server={0};Database={1};Integrated Security={2}",
                   Properties.ProjectSettings.Default.MsSqlServer,
                   Properties.ProjectSettings.Default.MsSqlDbName,
                   Properties.ProjectSettings.Default.MsSecurity);

        /// <summary>
        /// metoda prin care se definesc variantele de browser ce pot fi alese in clasa Base Test
        /// </summary>
        /// <param name="DriverType"></param>
        /// <returns></returns>
        public static IWebDriver GetDriver(Browsers DriverType)
        {
            IWebDriver driver = null;

            switch (DriverType)
            {
                case Browsers.Firefox:
                    {
                        FirefoxProfile profile = new FirefoxProfile();
                        profile.SetPreference("acceptsInsecureCerts", true);
                        profile.AcceptUntrustedCertificates = true;
                        FirefoxOptions fo = new FirefoxOptions();
                        return new FirefoxDriver(fo);
                    }
                case Browsers.Chrome:
                    {
                        ChromeOptions co = new ChromeOptions();
                        return new ChromeDriver(co);
                    }
                default:
                    {
                        Console.WriteLine("Driver selected not supported!");
                        break;
                    }


            }
            return driver;

        }
        /// <summary>
        /// metode folosite pentru citirea datelor din fisierele cu date pentru teste
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string ReadFile(string file)
        {

            return File.ReadAllText(file);

        }
        /// <summary>
        /// interpretarea datelor din fisierele cu date
        /// </summary>
        /// <param name="xml_text"></param>
        /// <returns></returns>
        public static Account DeserilizeXml(string xml_text)
        {
            // Root name = numele fisierului xml

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Account"
            };
            StringReader sr = new StringReader(xml_text);
            XmlSerializer xs = new XmlSerializer(typeof(Account), xRoot);
            return (Account)xs.Deserialize(sr);

        }
        /// <summary>
        /// citirea si interpretarea datelor din DB
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static System.Data.DataTable GetDataTable(string query, string tableName)
        {
            System.Data.DataTable tbl;
            using (SqlConnection conn = new SqlConnection(msConString))
            {
                SqlDataAdapter sda = new SqlDataAdapter(query, conn);
                DataSet ds = new DataSet(tableName);
                sda.Fill(ds, tableName);
                tbl = ds.Tables[tableName];
            }
            return tbl;
        }

        public static IEnumerable<LoginDataCsv> GetDbCredentials()
        {
            string query = "SELECT * FROM [testDb].[dbo].[Pr1_Credentials]";
            System.Data.DataTable tbl = GetDataTable(query, "Pr1_Credentials");
            foreach (DataRow rw in tbl.Rows)
            {
                string email = rw["Email"].ToString();
                string parola = rw["Parola"].ToString();
                yield return new LoginDataCsv(email, parola);
            }
        }

    }
}