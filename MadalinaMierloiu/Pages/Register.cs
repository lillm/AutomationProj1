﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Pages
{
    /// <summary>
    /// declararea si identificarea elementelor de pe pagina de register
    /// </summary>
    public class Register
    {
        
        public IWebDriver driver;
        IWebElement Prenume => driver.FindElement(By.Id("firstname"));
        IWebElement Nume => driver.FindElement(By.Id("lastname"));
        IWebElement Email => driver.FindElement(By.Id("email_address"));
        IWebElement Parola1 => driver.FindElement(By.Id("password"));
        IWebElement Parola2 => driver.FindElement(By.Id("confirmation"));
        IWebElement Captcha => driver.FindElement(By.XPath("//*[@id='recaptcha - anchor']/div[5]"));
        IWebElement Captcha2 => driver.FindElement(By.XPath("//*[@id='form - validate']/div[3]/button/span/span"));
        IWebElement el => driver.FindElement(By.CssSelector("#form-validate > div.buttons-set > p.required"));

        /// <summary>
        /// constructor driver
        /// </summary>
        /// <param name="driver"></param>
        public Register (IWebDriver driver)
        {
            this.driver = driver; 
        }

        /// <summary>
        /// metoda care completeaza campurile formularului de inregistrare
        /// </summary>
        /// <param name="prenume"></param>
        /// <param name="nume"></param>
        /// <param name="mail"></param>
        /// <param name="parola"></param>
        public void Regist(string prenume, string nume, string mail, string parola)
        {

            Prenume.Clear();
            Prenume.SendKeys(prenume);
            Nume.Clear();
            Nume.SendKeys(nume);
            Email.Clear();
            Email.SendKeys(mail);
            Parola1.Clear();
            Parola1.SendKeys(parola);
            Parola2.Clear();
            Parola2.SendKeys(parola);
            
        }

        /// <summary>
        /// metoda care verifica daca sunt marcate campurile obligatorii
        /// </summary>
            public void check()
            {
            Assert.AreEqual("* Campuri obligatorii", el.Text);
            }
        
    }
}
