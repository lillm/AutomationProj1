﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Pages
{
    public class Machete
    {
        /// <summary>
        /// declararea si identificarea elementelor de pe pagina cu machete
        /// </summary>
        public IWebDriver driver;
                
        IWebElement Filtru2 => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#target-to-hide > div.block.block-layered-nav > div.block-content > dl > dd:nth-child(2) > ol > li:nth-child(3) > span > a")));
        IList<IWebElement> Rezultate => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElements(By.ClassName("category-page-dimension")));
        IList<IWebElement> tag => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElements(By.ClassName("out-of-stock-label")));

        /// <summary>
        /// constructor driver
        /// </summary>
        /// <param name="driver"></param>
        public Machete(IWebDriver driver)
        {
            this.driver = driver;
        }

        /// <summary>
        /// metoda care aplica filtru
        /// </summary>
        public void Filter()
        {
           Filtru2.Click();
            Thread.Sleep(3000);
         }

        /// <summary>
        /// metoda care verifica daca sunt produse in stoc si afiseaza numarul lor
        /// </summary>
        public void Verify()
        {
            double v;
            
            v = Rezultate.Count - tag.Count;
            Assert.AreEqual(v, tag.Count);
            Console.WriteLine(Rezultate.Count + " obiecte gasite din care "+ v + " in stoc");
            
        }
    }
}
