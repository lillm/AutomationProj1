﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Pages
{
    public class Search
    {
        /// <summary>
        /// declararea si identificarea elementelor de pe pagina Search
        /// </summary>
        public IWebDriver driver;
        IWebElement Camp => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.Id("search")));
        IWebElement Rezultat1 => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.ClassName("hover-image")));
        IWebElement Adauga => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#product-addtocart-button > span")));
        IWebElement Cos => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#cartHeader > span > div > span.cos_de_cumparaturi")));
        IWebElement Favorit => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("body > div.content-wrapper > div > div > div > div:nth-child(3) > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.col-main > div:nth-child(4) > div > div > div > div.row.clearfix.padding_border > aside > div.wishlist_produc_page > p > a")));
        IWebElement Pret1 => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#product-price-5040 > span")));
        IWebElement Pret2 => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#mini-cart > li > div.product-details > div > span")));

        /// <summary>
        /// constructor driver
        /// </summary>
        /// <param name="driver"></param>
        public Search(IWebDriver driver)
        {
            this.driver = driver;
        }

        /// <summary>
        /// metoda care efectuaza cautarea unui obiect
        /// </summary>
        /// <param name="obiect"></param>
        public void Cauta(string obiect)
        {
            Camp.Clear();
            Camp.SendKeys(obiect);
            Camp.Submit();
        }
        /// <summary>
        /// metoda care adauga obiectul gasit in cos
        /// </summary>
        public void Add()
        {
            Rezultat1.Click();
            Thread.Sleep(3000);
            Adauga.Click();
        }

        /// <summary>
        /// metoda care adauga un produs in lista de favorite
        /// </summary>
        public void Fav()
        {
            Rezultat1.Click();
            Favorit.Click();
        }




        public void Ver()
        {
            Actions action = new Actions(driver);
            Thread.Sleep(3000);
            action.MoveToElement(Cos).Perform();
            Thread.Sleep(3000);
            Assert.AreEqual(Pret1.Text, Pret2.Text);

        }


    }
}

