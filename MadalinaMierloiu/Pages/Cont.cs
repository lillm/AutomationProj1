﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Pages
{
    public class Cont
    {
        /// <summary>
        /// declararea si identificarea elementelor de pe pagina My Account
        /// </summary>
        public IWebDriver driver;
       
        IWebElement Edit1 => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("body > div.content-wrapper > div > div > div:nth-child(3) > div > div > div > div.col-xs-12.col-sm-8.col-md-9.col-lg-9.col-main > div > div > div.box-account.box-info > div.row.clearfix > div.col-xs-12.col-sm-12.col-md-6.col-lg-6.alpha > div > div.box-title > a > i")));
        IWebElement Prenume2 => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#firstname")));
        IWebElement Salveaza => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#form-validate > div.buttons-set > button > span > span")));
        IWebElement Username => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.ClassName("customer-name")));

        /// <summary>
        /// constructor driver
        /// </summary>
        /// <param name="driver"></param>
        public Cont(IWebDriver driver)
        {
            this.driver = driver;
        }
        /// <summary>
        /// metoda de schimbare a prenumelui
        /// </summary>
        /// <param name="nou"></param>
        public void Change(string nou)
        {
            Edit1.Click();
            Prenume2.Clear();
            Prenume2.SendKeys(nou);
            Salveaza.Click();
         }

        /// <summary>
        /// metoda care verifica daca numele a fost schimbat conform actiunii anterioare
        /// </summary>
        /// <param name="nou"></param>
        public void Verifica(string nou)
        {
            Assert.AreEqual(nou, Username.Text);
        }


    }
}
