﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Pages
{
    /// <summary>
    /// declararea si identificare elementelor de pe pagina Favorite
    /// </summary>
    public class Favorite
    {
        public IWebDriver driver;
        IWebElement AdaugaInCos => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#wishlist-view-form > fieldset > div > button.button.btn-add > span > span")));
        IWebElement Inchide => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#v-cookielaw > div.v-actions > a.v-button.v-accept")));
        IWebElement Cantitate => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#qty-1490567774")));
        IWebElement Total => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#cart-form > div > div > div > div > div > form > div.col-xs-12.col-sm-12.col-md-12 > div.row.product-row-cart > div:nth-child(6) > div > span > span.price")));
        IWebElement PretUnit => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#cart-form > div > div > div > div > div > form > div.col-xs-12.col-sm-12.col-md-12 > div.row.product-row-cart > div.col-xs-4.col-sm-2.col-md-2.no-padding-sm.no-padding-md.price-wrapper.mutat_drepta_cart > div > span > span")));


        /// <summary>
        /// constructor driver
        /// </summary>
        /// <param name="driver"></param>
        public Favorite(IWebDriver driver)
        {
            this.driver = driver;
        }
        /// <summary>
        /// metoda care adauga elementele din lista de favorite in cos 
        /// </summary>
        public void Add()
        {
            Inchide.Click();
            AdaugaInCos.Click();
        }
        /// <summary>
        /// metoda care verifica daca pretul total e corect
        /// </summary>
        public void Compara()
        {

            Assert.AreEqual(Total.Text, (float.Parse(Cantitate.Text)) * float.Parse(PretUnit.Text));
        }
    }
}
