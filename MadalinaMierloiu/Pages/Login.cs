﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Pages
{
    /// <summary>
    /// declararea si identificarea elementelor de pe pagina de login
    /// </summary>
    public class Login
    {
        public IWebDriver driver;
        IWebElement Buton1 => driver.FindElement(By.LinkText("Login"));
        IWebElement Email => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.Id("email")));
        IWebElement Parola => driver.FindElement(By.Id("pass"));
        IWebElement Buton2 => driver.FindElement(By.XPath("//*[@id='send2']/span/span"));

        /// <summary>
        /// constructor driver
        /// </summary>
        /// <param name="driver"></param>
        public Login(IWebDriver driver)
        {
           this.driver = driver;
        }
        /// <summary>
        /// metoda care efectueaza loginul
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="parola"></param>
        public void Log(string mail, string parola)
        {
            Buton1.Click();
            Email.Clear();
            Email.SendKeys(mail);
            Parola.Clear();
            Parola.SendKeys(parola);
            Buton2.Click();

        }
    }
}
