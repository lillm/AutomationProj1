﻿using MadalinaMierloiu.DataMap;
using MadalinaMierloiu.Other;
using MadalinaMierloiu.Pages;
using MadalinaMierloiu.Test_References;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Tests

{
    public class Flow5 : BaseTest
    {
        /// <summary>
        /// test care efectueaza login, cauta un produs, il adauga la favorite
        /// adauga produsele din favorite in cos si verifica daca totalul pretului din cos e egal cu numarul de obiecte * pretul lor unitar
        /// </summary>
        [Test]
        public void AdaugaLaFavorite()
        {
            driver.Navigate().GoToUrl("https://magazinulcolectionarului.ro/");
            Login log = new Login(driver);
            string xml_text = Utils.ReadFile(@"C:\Users\Madalina\source\repos\MadalinaMierloiu\MadalinaMierloiu\Test Data\ValidLogin.xml");
            Account Id = Utils.DeserilizeXml(xml_text);
            log.Log(Id.Email, Id.Parola);
            Thread.Sleep(3000);
            Search fl5a = new Search(driver);
            fl5a.Cauta("tren");
            fl5a.Fav();

            Favorite fl5b = new Favorite(driver);
            fl5b.Add();
            
            fl5b.Compara();

        }
    }
}
