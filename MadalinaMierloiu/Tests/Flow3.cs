﻿using MadalinaMierloiu.DataMap;
using MadalinaMierloiu.Other;
using MadalinaMierloiu.Pages;
using MadalinaMierloiu.Test_References;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Tests

{
    public class Flow3 : BaseTest
    {
        /// <summary>
        /// test care efectueaza login cu date din xml, cauta un produs, retine pretul, il adauga in cos 
        /// si verifica daca pretul din cos(hover view) este egal cu cel retinut anterior
        /// </summary>
        [Test] 
        public void LoginTest()
        {
            driver.Navigate().GoToUrl("https://magazinulcolectionarului.ro/");
            Login log = new Login(driver);
            string xml_text = Utils.ReadFile(@"C:\Users\Madalina\source\repos\MadalinaMierloiu\MadalinaMierloiu\Test Data\ValidLogin.xml");
            Account Id = Utils.DeserilizeXml(xml_text);
            log.Log(Id.Email, Id.Parola);
            Thread.Sleep(3000);
            Search fl3 = new Search(driver);
            fl3.Cauta("tren");
            fl3.Add();
            Thread.Sleep(3000);
            fl3.Ver();

        }
    }
}
