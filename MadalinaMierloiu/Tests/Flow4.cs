﻿using MadalinaMierloiu.DataMap;
using MadalinaMierloiu.Other;
using MadalinaMierloiu.Pages;
using MadalinaMierloiu.Test_References;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Tests

{
    public class Flow4 : BaseTest
    {
        /// <summary>
        /// test care efectueaza un filtru si verifica produsele in stoc din rezultatele afisate
        /// </summary>
        [Test]
        public void Filtrare()
        {
            driver.Navigate().GoToUrl("https://magazinulcolectionarului.ro/machete");
            Machete mac = new Machete(driver);
            mac.Filter();
            mac.Verify();
        }
    }
}
