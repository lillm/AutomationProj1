﻿using MadalinaMierloiu.Other;
using MadalinaMierloiu.Pages;
using MadalinaMierloiu.Test_References;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MadalinaMierloiu.DataMap;
using ConsoleApp1.Other.DataMaps;

namespace MadalinaMierloiu.Tests_Data
{
    public class Flow1 : BaseTest
    {
        /// <summary>
        /// test care efectueaza completarea campurilor de pe pagina de inregistrare cu date dintr-un xml
        /// </summary>
        [Test]
        public void RegisterTest()
        {

            driver.Navigate().GoToUrl("https://magazinulcolectionarului.ro/customer/account/create/");

            Register reg = new Register(driver);
            string xml_text = Utils.ReadFile(@"C:\Users\Madalina\source\repos\MadalinaMierloiu\MadalinaMierloiu\Test Data\XMLFile.xml");
            Account Id = Utils.DeserilizeXml(xml_text);
            reg.Regist(Id.Prenume, Id.Nume, Id.Email, Id.Parola);
            Thread.Sleep(3000);

            reg.check();
        }


            /// <summary>
            /// metoda care efectueaza logarea cu date din DB
            /// </summary>
            /// <param name="currentData"></param>

        [Test, TestCaseSource(typeof(Utils), "GetDbCredentials")]
        public void DbTest(LoginDataCsv currentData)
        {
            driver.Navigate().GoToUrl("https://magazinulcolectionarului.ro/");
            Login lp = new Login(driver);
            lp.Log(currentData.Email, currentData.Parola);

            Thread.Sleep(3000);
         }

    }
    }

