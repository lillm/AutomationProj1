﻿using ConsoleApp1.Other.DataMaps;
using MadalinaMierloiu.DataMap;
using MadalinaMierloiu.Other;
using MadalinaMierloiu.Pages;
using MadalinaMierloiu.Test_References;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaMierloiu.Tests
{
    public class Flow2 : BaseTest
    {
        /// <summary>
        /// test care efectueaza login, accesarea My Account, modificarea prenumelui si verificarea noului prenume
        /// </summary>
        [Test]
        public void LoginTest()
        {
            driver.Navigate().GoToUrl("https://magazinulcolectionarului.ro/");
            Login log = new Login(driver);
            string xml_text = Utils.ReadFile(@"C:\Users\Madalina\source\repos\MadalinaMierloiu\MadalinaMierloiu\Test Data\ValidLogin.xml");
            Account Id = Utils.DeserilizeXml(xml_text);
            log.Log(Id.Email, Id.Parola);
            Thread.Sleep(3000);

            Cont acc = new Cont(driver);
            acc.Change("madalina");
            acc.Verifica("madalina");
            

        }
    }
}
