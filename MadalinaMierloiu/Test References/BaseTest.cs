﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MadalinaMierloiu.Other;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MadalinaMierloiu.Other.EnumItems;
/// <summary>
/// clasa de baza ce contine initializari si definitii folosite in toate testele
/// </summary>
namespace MadalinaMierloiu.Test_References
{
    [TestFixture]

    public class BaseTest
    {
        /// <summary>
        /// declararea varibilelor:
        /// - web driver-ul
        /// - raportul cu rezultatele testelor
        /// - locatia si numele fisierului cu raportul
        /// </summary>
        public static IWebDriver driver;

        protected ExtentReports extent;
        protected ExtentTest test;

        public string dir;
        public string fileName;

        [OneTimeSetUp]

        /// <summary>
        /// crearea raportului cu rezultatele testelor in format html
        /// </summary>
        public void BeforeAll()
        {
            dir = TestContext.CurrentContext.TestDirectory;
            Console.WriteLine(dir);
            fileName = this.GetType().ToString();
            Console.WriteLine(fileName);
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(String.Format("{0}\\{1}.html", dir, fileName));
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }

        [OneTimeTearDown]
        public void AfterAll()
        {
            extent.Flush();
        }

        /// <summary>
        /// initializare web driverului si lansarea browserului, la alegere, maximized
        /// </summary>
        [SetUp]
        public void SetupTests()
        {
            Console.WriteLine("This is the setup ");
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--start-maximized");
            driver = new ChromeDriver(options);
            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
        }
        /// <summary>
        /// maparea rezultatului testului cu statusul din raport
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>

        public Status MapStatuses(TestStatus status)
        {
            switch (status)
            {
                case TestStatus.Inconclusive:
                    return Status.Warning;
                case TestStatus.Skipped:
                    return Status.Skip;
                case TestStatus.Passed:
                    return Status.Pass;
                case TestStatus.Warning:
                    return Status.Warning;
                case TestStatus.Failed:
                    return Status.Fail;
                default:
                    return Status.Fatal;
            }
        }
        /// <summary>
        /// crearea raportului si a unui screen shoot in cazul in care testul pica
        /// </summary>
            [TearDown]
        public void TearDownTests()
        {
            Console.WriteLine("This is the teardown");

            TestStatus status = TestContext.CurrentContext.Result.Outcome.Status;
            string stk = TestContext.CurrentContext.Result.StackTrace;
            stk = string.IsNullOrEmpty(stk) ? "" : String.Format("\nThe stack trace is : \n{0}", stk);
            Status logstatus = MapStatuses(status);
            test.Log(logstatus, "Test ended with " + logstatus + stk);
            if (logstatus == Status.Fail)
            test.Fail("Test failed", MediaEntityBuilder.CreateScreenCaptureFromPath(String.Format("{0}\\screeenshot.png", dir)).Build());

            extent.Flush();
            driver.Quit();
            

        }

    }
}
